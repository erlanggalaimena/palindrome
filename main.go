package main

import (
	"fmt"

	"gitlab.com/erlanggalaimena/palindrome/service"
)

func main() {
	fmt.Println(service.IsPalindrome(121))
	fmt.Println(service.IsPalindrome(-121))
	fmt.Println(service.IsPalindrome(10))
}
