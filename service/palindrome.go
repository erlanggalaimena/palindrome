package service

func IsPalindrome(x int) bool {
	if x < 0 {
		return false
	}

	y := 0
	z := x

	for ; z > 0; z /= 10 {
		y = (y * 10) + (z % 10)
	}

	return x == y
}
