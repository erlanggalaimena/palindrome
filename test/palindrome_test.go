package test

import (
	"fmt"
	"testing"

	"gitlab.com/erlanggalaimena/palindrome/service"
)

func TestPalindrome(t *testing.T) {
	fmt.Println("Test Case 1 Processed")
	testCase1 := service.IsPalindrome(121)
	if !testCase1 {
		t.Error("Test Case 1 Fail")
	} else {
		fmt.Println("Test Case 1 Success")
	}

	fmt.Println("Test Case 2 Processed")
	testCase2 := service.IsPalindrome(-121)
	if testCase2 {
		t.Error("Test Case 2 Fail")
	} else {
		fmt.Println("Test Case 2 Success")
	}

	fmt.Println("Test Case 3 Processed")
	testCase3 := service.IsPalindrome(10)
	if testCase3 {
		t.Error("Test Case 3 Fail")
	} else {
		fmt.Println("Test Case 3 Success")
	}
}
